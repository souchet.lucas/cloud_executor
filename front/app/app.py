import json
from flask import Flask, request
import docker
from dotenv import load_dotenv
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, PickleType, Text, Float, DateTime, create_engine
#

import asyncio
import os
import subprocess
from sqlalchemy.orm import sessionmaker#
import datetime

app = Flask(__name__)
load_dotenv()

DATABASE_URI = 'postgresql://'+os.getenv("POSTGRES_USER")+":"+os.getenv("POSTGRES_PASSWORD")+"@db/"+os.getenv("POSTGRES_DB")
app.config['SQLALCHEMY_DATABASE_URI'] = DATABASE_URI
db = SQLAlchemy(app)
docker_client = docker.from_env()

try:
    engine = create_engine(DATABASE_URI)
except Exception as e:
    app.logger.error(e)
Base = declarative_base()
#
#

def recreate_database():
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
recreate_database()
s = Session()
#

class Job(Base):
    __tablename__ = 'jobs'

    def __init__(self, dockerfile):
        self.dockerfile = dockerfile
        self.status = "IN_PROGRESS"
        self.perf = None
        self.date = datetime.datetime.now()

    id = Column(Integer, primary_key=True)
    dockerfile = Column(Text, nullable=False)
    status = Column(PickleType, nullable=False)
    perf = Column(Float, nullable=True)
    date = Column(DateTime)

    def __repr__(self):
        return '<Job %d>' % self.id

async def build_image(json_dockerfile, job_id):
    path = os.path.join("/tasks",str(job_id))
    os.makedirs(path)
    filepath = os.path.join(path, "Dockerfile")
    with open(os.path.join(filepath), "wt") as f:
        f.write(json_dockerfile)
    image_name = "image" + str(job_id)
    try:
        (image, logs_json) = docker_client.images.build(path=path, tag=image_name)
    except Exception as e:
        app.logger.error(e)
        return
    #Scan does not seem to be part of the lib, hence we launch it with the system
    status = subprocess.run(["docker","scan", image_name], stderr=subprocess.PIPE, text=True)
    if False:#Define a regex for status parsing 
        return "FAILED"
    #TAG and push the images
    # Use kubectl to start the containers with the image,


@app.route('/run_job', methods=['POST'])
def run_job():
    try:
        str_dockerfile = request.get_json()["dockerfile"]
        job = Job(str_dockerfile)
        s.add(job)
        s.commit()
        app.logger.info("running build_image")
    
        asyncio.run(build_image(str_dockerfile, job.id))
    except Exception as e:
        app.logger.error("error while building image "+ str(e))
    #app.logger.info("image built")
    return str(job.id)

@app.route('/')
def home():
    print(DATABASE_URI)
    try:
        app.logger.info(DATABASE_URI)
        res = s.query(Job).first()
        app.logger.info(res)
    except Exception as e:
        app.logger.error("home error: " + e)
    return "OK"

@app.route('/add')
def add():
    print(DATABASE_URI)
    try:
        app.logger.info(DATABASE_URI)
        #
        j = Job("blabla")
        s.add(j)
        s.commit()
    except Exception as e:
        app.logger.error("add error: " + str(e))
    return "OK"

@app.route('/get_job/<job_id>', methods=['GET'])
def get_job(job_id):
    res = s.query(Job).filter_by(id=job_id).first()
    app.logger.info("res: ", res.dockerfile)
    return "OK"
  

if __name__ == '__main__':
    app.run(host ='0.0.0.0', port = 8080)