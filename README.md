# Note:
The code is not functional as-is.
There is a skeleton for the Flask server, some docker and docker-compose files for local containerization managers and some deployment and services yaml for the kubernetes management of the executor.

# Goal

Server that provides REST API to:
- Pass the dockerfile and retrieve a job ID
- Retrieve status and performance from a particular job

When a dockerfile is provided, an image is built, and a container is started with this image

# Components

- Flask web server for REST API
- Nginx for routing to api service
- Postgresql database to store jobs, dockerfile, status, perf
- kubernetes cluster (executor) to run jobs


## API Server

The Server provides two functions:

### POST /run_job
> headers: {"token":token}
> data: {"dockerfile":"FROM ubuntu:latest
> \# train machine learning model
> \# save performances
> CMD echo ʻ{\"perf\":0.99}ʼ > /data/perf.json}"}

2. Create an entry in the db:
> "id": ID
> "dockerfile": dockerfile
> "status": BUILD_IN_PROGRESS
> "perf": ""
Return the ID of the job

3. build the docker container then set status to UPLOAD_IN_PROGRESS
> echo \$dockerfile > Dockerfile
> IMAGE_ID = IMAGE_\$ID
> docker build -t CLOUD_IMAGE:\$IMAGE_ID

4. Check the container for vulnerabilities
> docker scan CLOUD_IMAGE:$IMAGE_ID
if a vulnerability is found, set the status in db to "FAILED" and stop

5. Tag and push the image to a registry then set status to JOB_STARTING
> docker login -u cloud_executor -p \$TOKEN \$DOCKER_REGISTRY
> docker push CLOUD_IMAGE:$IMAGE_ID

6. Start a VM with this image
> NAMESPACE=NAMESPACE_$IMAGE_ID
> kubectl create ns \$NAMESPACE
> kubectl create secret -n \$NAMESPACE docker-registry --docker-server="\$DOCKER_REGISTRY" --docker-username="cloud_executor" --docker-password="\$TOKEN"
> sed -e "s/TO_REPLACE/\$IMAGE_ID/g" templates/deployment.yaml | tee ".generated/deployment.yaml"
> kubectl apply -f .generated/deployment.yaml
> kubectl apply -f executor/kubernetes/service.yaml

7. When image is started, set status to JOB_STARTING in db
> until kubectl get pods -o=jsonpath='{.items[?(@.metadata.labels.name==\$IMAGE_ID)].status.conditions[*].status}' -n \$NAMESPACE | grep -v False; do sleep 5; done
> \# SQL request

8. Monitor in a loop the file /data/perf.json to check when it is present
We could sync of using ksync to watch the /data/perf.json being written on the pod
> ksync create --pod=my-pod perfs/$IMAGE_ID/perf.json /data/perf.json
> ksync watch

9. Read /data/perf.json, write the perf in db and set status SUCCESS
> \# SQL REQUEST

10. Destroy the container to save resources
> kubectl delete ns \$NAMESPACE

### GET /get_job/<job_id>
1. Retrieve information from the job in db
2. Return JSON containing information
> {"id":"22834", "status":"FAILED"}
> OR
> {"id": "8289", "status": "SUCCESS", "perf": "0.832"}

# Issues / Questions

## Security concerns
In this proposal, the docker build of images is run on the server. This might be ineffective and dangerous.
An improvement could consist in running the build of image in a separate pod in the kubernetes cluster in an isolated environment.

## Docker in Docker
Running the docker build image in a docker container requires Docker-in-docker pattern.
I know this pattern to lead to issue where the docker container concept is a bit different.
I am not sure what the state-of-the-art methods for solving this are.
In this project, I add the docker sock as a volume to the web server container.
I am not sure how it would scale

## scaling
The web server and the db run locally in its own container, for availability, scalability and reliability, it should be launched on a kubernetes cluster behind a load balancer

## Nginx
I haven't really deep dived into Nginx, but it is included in the docker image from dockerhub that contains Flask. It could be used to route messages to the server and the db, as well as debug containers (db admin, monitoring, ...).
I never used it as a load balancer, but I know it is possible. I am not sure it would be relevant with regards to kubernetes orchestrator.

## Cloud providers
All of this infrastructure could be deployed on a cloud infrastructure such as AWS or Google Cloud Platform.
One could use the Cloud provider respective SDK to setup the Kubernetes clusters and provision the compute engines (or EC2 instances).
Setting up interfaces with the cloud providers was too time consuming here, but I think we could use:
- database and associated storage
- Kubernetes integration support and cluster management
- Virtual network support for security of the data between the Internet, the server and the executors
- Docker Image repositories (public or private) for containerized application launching
- Frontend hosting for management and visualization of the ongoing tasks and their results

